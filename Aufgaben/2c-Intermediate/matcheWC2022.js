const url = "https://worldcupjson.net/";

// Diese Funktion holt die Daten aus der API und gibt sie als JSON zurück.
async function getMatches() {
    const response = await fetch(url + "/matches");
    return await response.json();
}

// Diese Funktion mappt die Daten aus der API auf ein neues Objekt. Die Objekte werden in einem Array gespeichert und zurückgegeben. Ausserdem wird bestimmt welche Objekte gemappt werden sollen.
async function mapMatches(){
    const matches = await getMatches()
    return matches.map(match => {
        return {
            id: match.id,
            venue: match.venue,
            location: match.location,
            status: match.status,
            attendance: match.attendance,
            stage_name: match.stage_name,
            home_team_country: match.home_team_country,
            away_team_country: match.away_team_country,
            datetime: match.datetime,
            winner: match.winner,
            winner_code: match.winner_code,
            home_team: match.home_team,
            away_team: match.away_team,
            last_checked_at: match.last_checked_at,
            last_changed_at: match.last_changed_at
        }
    });
}

// Hier muss das console.log im nachhinein erfolgen, da die Funktion asynchron ist.
mapMatches().then(data => console.log(data));
