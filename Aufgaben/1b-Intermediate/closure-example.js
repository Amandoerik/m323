// Bei dieser Funktion wird angegeben welcher Status die User haben.
function createMessage(definedStatus) {
    return function addSecondMessage(users) {
        return definedStatus + users;
    }
}

let defineStatusActive = createMessage('Have the status active: ');
let defineStatusInactive = createMessage('Have the status inactive: ');

console.log(defineStatusActive(['Max', ' Moritz', ' Peter']));
console.log(defineStatusInactive(['Michael', ' Alex', ' Sandra']));
