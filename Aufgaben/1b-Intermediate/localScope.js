// Da die Variable lokal ist, kann sie nur innerhalb der Funktion verwendet werden.
function addWorld() {
    let localString = "Hello";
    localString += " World";
    console.log(localString);
}

// Ausserhalb kann die Variable nicht verwendet werden, somit ist console.log(localString); undefined.
addWorld();
console.log(localString);

