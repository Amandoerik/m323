// Da die zweite Funktion sich innerhalb der Funktion befindet, in der die Variable instanziiert wurde, 
// kann diese immer noch verwendet. Sie befindet sich somit noch im Scope der Funktion.
function createHello() {
    let localString = "Hello";

    function addWorld() {
        localString += " World";
        console.log(localString);
    }
    
    return addWorld;
}

// Ausserhalb kann die Variable nicht verwendet werden, somit ist console.log(localString); undefined.
let hi = createHello();
hi();
