// Hierbei handelt es sich um eine globale Variable
let globalString = "Hello";

// Da die Variable global ist, kann sie innerhalb der Funktion verwendet werden.
function addWorld() {
    globalString += " World";
    console.log(globalString);
}

// Ebenso kann sie ausserhalb der Funktion verwendet werden
console.log(globalString);
addWorld();

// Bei beiden gibt keinen Fehler, da die Variable, wie bereits erwähnt, global ist.
