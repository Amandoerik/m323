// -- Aufgabe 1 --

function multiply(basisNum, startNum, endNum) {
    const result = [];
    for (let i = startNum; i <= endNum; i++) {
        result.push(i * basisNum);
    }
    return result;
}

console.log(multiply(10, 0, 3)); // [0,10,20,30]
console.log(multiply(20, 0, 3)); // [0,20,40,60]


// -- Aufgabe 2 --

function multiplyCurry(basisNum) {
    return function (startNum, endNum) {
        const result = [];
        for (let i = startNum; i <= endNum; i++) {
            result.push(i * basisNum);
        }
        return result;
    }
}

const rowBy10 = multiplyCurry(10);
console.log(rowBy10(0, 3)); // [0, 10, 20, 30]

const rowBy20 = multiplyCurry(20);
console.log(rowBy20(0, 3)); // [0, 20, 40, 60]