const fs = require('fs');

function loadData(filename) {
    const data = fs.readFileSync(filename, 'utf8').split('\n').map((line, index) => ({ index, line }));
    let currentIndex = 0;

    return function (linesCount) {
        const selectedLines = data
            .filter(item => item.index >= currentIndex && item.index < currentIndex + linesCount)
            .map(item => item.line);

        currentIndex += linesCount;

        if (selectedLines.length === 0) {
            return 'no more data';
        }

        return selectedLines.join('\n');
    };
}

var dataLoader = loadData('data.txt');

var page1 = dataLoader(5);
console.log(page1);
var page2 = dataLoader(5);
console.log(page2);
var page3 = dataLoader(5);
console.log(page3);

// das txt file befand sich im gleichen Ordner wie die js Datei