var grades = [1.2, 3.5, 5, 3, 4.75, 6, 5.25];

function average(grades){
    if (!grades) {
        return "invalid input";
    }
    return grades.reduce((acc, grade) => acc + grade, 0) / grades.length;
}

function min(grades){
    if (!grades) {
        return "invalid input";
    }
    return Math.min(...grades.filter(x => x != undefined));
}

function max(grades){
    if (!grades) {
        return "invalid input";
    }
    return Math.max(...grades.filter(x => x != undefined));
}

console.log("Average: " + average(grades));
console.log("Minimum: " + min(grades));
console.log("Maximum: " + max(grades));
