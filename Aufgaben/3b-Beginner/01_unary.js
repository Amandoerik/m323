// unary() ist eine Funktion, die eine Funktion als Argument erhält und eine neue Funktion zurückgibt, die nur ein Argument akzeptiert.
// Sie vehindert parseInt daran, mehr als ein Argument zu erhalten.
function unary(fn) {
    return function onlyOneArg(arg) {
        return fn(arg);
    };
}

const nums = [1, 2, 3, 4, 5, 2.3, 5.559];
const numsInStrs = ["1", "2", "3", "4", "5", "2.3", "5.559"];

console.log(nums.map(unary(parseInt)));
console.log(nums.map(parseInt));
console.log(numsInStrs.map(unary(parseInt)));
console.log(numsInStrs.map(parseInt));

// Was macht die unary() Funktion und warum wird sie benötigt?
// Da map() 3 Argumente mitgibt aber parseInt nur 1 Argument akzeptiert, kann es zu Probleme kommen.
// Die unary Function vehindert parseInt daran, mehr als ein Argument zu erhalten.
