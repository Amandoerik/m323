# Rekursion

## Rekursion in Funktionaler Programmierung

Rekursion in der funktionalen Programmierung ist eine Methode, bei der eine Funktion sich selbst aufruft, um ein Problem zu lösen. Diese Technik ist besonders wichtig, da sie gut mit den Grundprinzipien der funktionalen Programmierung - wie Unveränderlichkeit und Zustandslosigkeit - zusammenspielt.

### Wichtigkeit der Rekursion in der Funktionalen Programmierung

1. **Unveränderliche Daten**: Rekursion passt gut zu unveränderlichen Datenstrukturen.
2. **Zustandslosigkeit**: Rekursive Funktionen speichern keinen Zustand zwischen den Aufrufen.
3. **Ausdrucksstärke**: Rekursion ermöglicht eine klare und knappe Problemlösung.
4. **Keine Seiteneffekte**: Seiteneffekte werden vermieden in dem Rekursion keine externen Zustände verändert.

### Ersetzen Iterativer Konzepte durch Rekursion

- **Einfachere Teilprobleme**: Ein Problem wird in kleinere Probleme aufgeteilt, bis zum einfachsten Fall.
- **Stapelverarbeitung**: Statt Schleifen werden Aufrufe auf dem Stapel abgelegt.
- **Akkumulatoren**: Diese helfen, Zwischenergebnisse zu speichern, um das Endergebnis zu erzielen.

### Vorteile

- Komplexe Probleme mit Recursion einfacher lösbar als Iteration
- Elegant und kompakt

### Nachteile

- Stack-Overflow bei falscher Umsetzung
- Komplizierter als iterative Lösungen

### Beispiel in JavaScript: Fakultät Berechnung

```javascript
function fibonacci(n) {
   if (n === 0 || n === 1) {
     return n;
   } else {
     return fibonacci(n - 1) + fibonacci(n - 2);
   }
}
  
console.log(fibonacci(10)); // [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]
```javascript
