// ------ Aufgabe 7 ------

function storeData(store,location,value) {
    store[location] = value;
}

function saveComment(comments, txt) {
    if (txt !== "") {
        var formattedText = getFormattedDateTime() + ": " + txt;
        storeData(comments, comments.length, formattedText);
    }
}

function trackEvent(evt) {
    if (evt.name !== undefined) {
        storeData( events, evt.name, evt );
    }
}

function getFormattedDateTime() {
    var now = new Date();
    return now.toLocaleString();
}

let comments = ["The current date and time is: "];

saveComment(comments, "Thank you");

console.log(comments);