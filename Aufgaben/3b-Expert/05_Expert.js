// ------ Aufgabe 5 ------

function compose(...fns) {
    // pull off the last two arguments
    var [ fn1, fn2, ...rest ] = fns.reverse();

    var composedFn = function composed(...args){
        return fn2( fn1( ...args ) );
    };

    if (rest.length == 0) return composedFn;

    return compose( ...rest.reverse(), composedFn );
}

const add1 = (a) => a + 1; 
const doubler = (a) => a * 2; 

const composedFn = compose(add1, doubler);
console.log(composedFn(5)); // Output: 11 -> Die Ausführung geschieht von rechts nach links

const composedFn2 = compose(doubler, add1);
console.log(composedFn2(5)); // Output: 12 -> Die Ausführung geschieht von rechts nach links
