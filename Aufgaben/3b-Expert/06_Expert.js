// ------ Aufgabe 6 ------

function compose(...fns) {
    // pull off the last two arguments
    var [ fn1, fn2, ...rest ] = fns.reverse();

    var composedFn = function composed(...args){
        return fn2( fn1( ...args ) );
    };

    if (rest.length == 0) return composedFn;

    return compose( ...rest.reverse(), composedFn );
}

const add1 = (a) => a + 1; 
const doubler = (a) => a * 2; 

const composedFn = compose(add1, doubler); 
console.log("Ausgabe mit compose: " + composedFn(5)); // Output: 11 -> Die Ausführung geschieht von **rechts** nach links


function pipe(...fns) {
    return function piped(result){
        var list = [...fns];

        while (list.length > 0) {
            // take the first function from the list
            // and execute it
            result = list.shift()( result );
        }

        return result;
    };
}

function reverseArgs(fn) {
    return function argsReversed(...args){
        return fn( ...args.reverse() );
    };
}

var pipe = reverseArgs( compose );
const pipeFn = pipe(doubler, add1);
console.log("Ausgabe mit pipe: " + pipeFn(5)); // Output: 11 -> Die Ausführung geschieht von **links** nach rechts