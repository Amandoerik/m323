// base code
["1","2","3"].map(parseInt);

/* Aufgabe 1: Erklären sie in eigenen Worten was der Code machen soll
 * Der Code soll aus dem String Array ["1","2","3"] ein Integer Array [1,2,3] machen. Dafür wird .map() verwendet, 
 * welches für jedes Element des Arrays die Funktion parseInt() aufruft. parseInt() wandelt einen String in einen Integer um.
*/

/* Aufgabe 2: Erklären sie in eigenen Worten warum es nicht funktioniert
 * parseInt() erwartet zwei Parameter: den zu konvertierenden String und die Basis, in der der String interpretiert werden soll.
 * Da .map() jedoch drei Parameter weitergibt (aktuelles Element, Index, Array), führt dies zu unerwarteten Ergebnissen.
*/

/* Aufgabe 3: Schreiben sie den Code so funktional um, das es funktioniert
 * parseInt() wird als Arrow Function aufgerufen, die den String als Parameter bekommt und die Basis 10 (diese ist standardmässig 10) setzt.
 * Nun bekommt parseInt() nur noch zwei Parameter die Basis und die Zahl einzeln und nicht als Element.
*/
["1","2","3"].map((x) => parseInt(x));
