const https = require('https');

const fetchData = (url) => new Promise((resolve, reject) => {
  https.get(url, (response) => {
    let data = '';
    response.on('data', (chunk) => (data += chunk));
    response.on('end', () => {
      try {
        resolve(JSON.parse(data));
      } catch (error) {
        reject(new Error('Error parsing JSON-data: ' + error.message));
      }
    });
  }).on('error', (error) => {
    reject(new Error('Error while retrieving data: ' + error.message));
  });
});

const countGoals = (matches) => {
  const addTeamGoals = (acc, team) => {
    const teamName = team?.country || 'Unknown Team';
    acc[teamName] = (acc[teamName] || 0) + team.goals;
    return acc;
  };

  return matches.reduce((acc, { home_team, away_team }) => {
    addTeamGoals(acc, home_team);
    addTeamGoals(acc, away_team);
    return acc;
  }, {});
};

const sortTeams = (goalCounts) =>
  Object.entries(goalCounts).sort((a, b) => b[1] - a[1]);

const getTopAndFlopTeams = (sortedTeams) => ({
  top5teams: sortedTeams.slice(0, 5),
  flop5teams: sortedTeams.slice(-5),
});

const findGameWithMostGoals = (matches) =>
  matches.reduce((maxGoalsGame, { home_team, away_team }) => {
    const totalGoals = home_team.goals + away_team.goals;
    return totalGoals > maxGoalsGame.goals ? { goals: totalGoals } : maxGoalsGame;
  }, { goals: 0 });

const findStadiumWithMostGoals = (matches) =>
  matches.reduce((stadiumGoals, { location, home_team, away_team }) => {
    const totalGoals = home_team.goals + away_team.goals;
    stadiumGoals[location] = (stadiumGoals[location] || 0) + totalGoals;
    return stadiumGoals;
  }, {});

const displayResults = ({ goalCounts, top5teams, flop5teams, mostGoalsPerGame, mostGoalsPerStadium }) => {
  console.log('Most goals per team:', goalCounts);
  console.log('\nTop 5 teams:', top5teams);
  console.log('\nFlop 5 teams:', flop5teams);
  console.log('\nGame with most goals:', mostGoalsPerGame);
  console.log('\nStadium with most goals:', mostGoalsPerStadium);
};

const main = async () => {
  try {
    const matches = await fetchData('https://worldcupjson.net/matches');
    if (!matches) return;

    const goalCounts = countGoals(matches);
    const sortedTeams = sortTeams(goalCounts);
    const { top5teams, flop5teams } = getTopAndFlopTeams(sortedTeams);
    const mostGoalsPerGame = findGameWithMostGoals(matches);
    const mostGoalsPerStadium = Object.entries(findStadiumWithMostGoals(matches))
      .reduce((max, entry) => (entry[1] > max[1] ? entry : max), ['', 0]);

    displayResults({ goalCounts, top5teams, flop5teams, mostGoalsPerGame, mostGoalsPerStadium });
  } catch (error) {
    console.error(error.message);
  }
};

main();