function anyAmountOfParameters(...args) {
    args.forEach(x => {
        if (x % 2 === 0) {
            console.log(x);
        }
    });
}
  
anyAmountOfParameters(1,2,4,5,6,7,8,9,10); // Gibt die geraden Zahlen züruck, anzahl der Parameter ist beliebig

function anyAmountOfParameters2() {
    for (let i = 0; i < anyAmountOfParameters2.arguments.length; i++){
        if (anyAmountOfParameters2.arguments[i] % 2 === 0){
            console.log(anyAmountOfParameters2.arguments[i]);
        }
    }
}

anyAmountOfParameters2(1,2,4,5,6,7,8,9,10); // Gibt die geraden Zahlen züruck, anzahl der Parameter ist beliebig