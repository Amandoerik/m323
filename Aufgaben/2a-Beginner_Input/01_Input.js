function defaultvalue (a = 1, b = 2) {
    return a + b;
}

console.log(defaultvalue(3, 4)); // 7
console.log(defaultvalue()); // 3, da a und b default-Werte haben