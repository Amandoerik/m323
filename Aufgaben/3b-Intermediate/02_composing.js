function words(str) {
    return String(str)
        .toLowerCase()
        .split(/\s|\b/)
        .filter(function alpha(v) {
            return /^[\w]+$/.test(v);
        });
}

function unique(list) {
    var uniqList = [];

    for (let v of list) {
        // value not yet in the new list?
        if (uniqList.indexOf(v) === -1) {
            uniqList.push(v);
        }
    }

    return uniqList;
}

// Diese Funktion zählt die Wörter von HTML File der Webseite digitec.ch 
fetch('https://www.digitec.ch')
    .then(response => response.text())
    .then(data => {
        var wordsFound = words(data);
        var wordsUsed = unique(wordsFound);
        console.log(wordsUsed);
    })
    .catch(error => console.error(error));
