// Berechnet die Fakultät einer Zahl
function getFaculty(n) {
    if (n === 0) {
        return 1n;
    }
    return BigInt(n) * getFaculty(--n);
}

// Testet die Funktion getFaculty()
function testGetFaculty(testNumber, result) {
    return getFaculty(testNumber) === result;
}

// Testfälle
console.log(testGetFaculty(3, 6n));
console.log(testGetFaculty(5, 120n));
console.log(testGetFaculty(10, 3628800n));
console.log(testGetFaculty(0, 1n));
console.log(testGetFaculty(1, 1n));
console.log(testGetFaculty(20, 2432902008176640000n));
console.log(testGetFaculty(30, 265252859812191058636308480000000n)); // Löst fehler aus, da die Zahl zu gross ist - Fix wurde mit BigInt() eingeführt
console.log(testGetFaculty(50, 30414093201713378043612608166064768844377641568960512000000000000n)); // Neuer Testcase nach fix - funktioniert
console.log(testGetFaculty(100, 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000n)); // Neuer Testcase nach fix - funkioniert