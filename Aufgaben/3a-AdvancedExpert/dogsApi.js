/*

GIBZ / M323

Kompetenz 3a Advanced + Expert

Sie haben eine bestehende Anbindung an die Hunde-API: https://dog.ceo/dog-api/

Ihr Ziel ist es verschiedene Funktionen zur Verfügung zu stellen:
1. Laden aller Hunderassen
2. Laden eines Random Bildes einer Hunderasse
3. Ausgabe aller Hunderassen in der Console
4. Eine Funktion die ein Array von Hunderassen annimmt und als Ergebnis eine Liste von Random Bilder für diese Hunderassen zurückgibt
5. Eine Funktion die die Subrassen einer eingegebenen Hunderasse als Liste zurückgibt

Sie sollen dabei das gelernte Anwenden:

u.a.:

- Asynchrones Programmieren
- Keine Seiteneffekte
- Eine Funktion macht nur etwas
- Currying für Funktionen mit gleicher Basis
- Keine Code-Redundanzen
- Saubere intention
- Ihr Code sollte dokumentiert sein
- Sprechende Variablennamen
- Sinnvoller Einsatz von var/let/const

Untenstehend finden Sie den Code der Ihr Vorgänger geschrieben hat. Leider hatte dieser noch nicht so viel Erfahrung und Sie müssen schauen,
welchen Code Sie verwenden können und was Sie ggf. neu schreiben müssen.

*/


const { get } = require("http");
const https = require("https");

/**
 * Aufgabe 1: Funktion zum Laden aller Hunderassen
 * @returns {Promise<Array>} Liste aller Hunderassen
 */
async function getDogBreeds() {
    return new Promise((resolve, reject) => {
        https.get('https://dog.ceo/api/breeds/list/all', (response) => {
            let chunks_of_data = [];

            response.on('data', (fragments) => {
                chunks_of_data.push(fragments);
            });

            response.on('end', () => {
                const response_body = Buffer.concat(chunks_of_data);
                const breeds = JSON.parse(response_body).message;
                const breedList = Object.keys(breeds);

                resolve(breedList);
            });

            response.on('error', (error) => {
                reject(error);
            });
        });
    });
}

/**
 * Aufgabe 2: Funktion zum Laden eines random Bildes einer Hunderasse
 * @param {string} breed - Hunderasse
 * @returns {Promise<string>} - URL des random Hundebildes
 */
async function getRandomBreedImage(breed) {
    return new Promise((resolve, reject) => {
        const url = `https://dog.ceo/api/breed/${breed}/images/random`;

        https.get(url, (response) => {
            let chunks_of_data = [];

            response.on('data', (fragments) => {
                chunks_of_data.push(fragments);
            });

            response.on('end', () => {
                const response_body = Buffer.concat(chunks_of_data);
                const image = JSON.parse(response_body).message;

                resolve(image);
            });

            response.on('error', (error) => {
                reject(error);
            });
        });
    });
}

/**
 * Aufgabe 3: Funktion zum Laden aller Hunderassen mithilfe der Funktion aus Aufgabe 1
*/
async function getCountOfDogBreeds() {
    const breeds = await getDogBreeds();
    return breeds.length;
}
console.log("\nAnzahl aller Hunderassen:", getCountOfDogBreeds());



/**
 * Aufgabe 5: Funktion zum Laden von random Bildern für ein Array von Hunderassen
 * @param {Array} breedList - Liste von Hunderassen
 * @returns {Promise<Array>} - Liste von zufälligen Hundebild-URLs
 */
async function getRandomImagesForBreeds(breedList) {
    const promises = breedList.map(async (breed) => await getRandomBreedImage(breed));
    return Promise.all(promises);
}


/**
 * Aufgabe 5: Funktion zum Laden der Subrassen einer Hunderasse
 * @param {string} breed - Hunderasse
 * @returns {Promise<Array>} - Liste der Subrassen
 */
async function getSubBreeds(breed) {
    return new Promise((resolve, reject) => {
        const url = `https://dog.ceo/api/breed/${breed}/list`;

        https.get(url, (response) => {
            let chunks_of_data = [];

            response.on('data', (fragments) => {
                chunks_of_data.push(fragments);
            });

            response.on('end', () => {
                const response_body = Buffer.concat(chunks_of_data);
                const subBreeds = JSON.parse(response_body).message || [];

                resolve(subBreeds);
            });

            response.on('error', (error) => {
                reject(error);
            });
        });
    });
}



// Testaufrufe:
(async () => {
    try {
        // ---- Aufgabe 1 ----
        const breeds = await getDogBreeds();
        console.log("\nAufgabe 1: Alle Hunderassen:", breeds);
        

        // ---- Aufgabe 2 ----
        const randomBreedImage = await getRandomBreedImage(breeds[0]);
        console.log(`\nAufgabe 2: Random Bild von der Hunderasse ${breeds[0]}:`, randomBreedImage);
        

        // ---- Aufgabe 3 ----
        // console.log("\nAnzahl aller Hunderassen:", breeds.length); // !!!wurde bereits in Aufgabe 3 ausgegeben


        // ---- Aufgabe 4 ----
        const randomImagesForBreeds = await getRandomImagesForBreeds(breeds.slice(0, 3));
        console.log("\nRandom Bilder einer Hunderrasse:", randomImagesForBreeds);


        // ---- Aufgabe 5 ----
        const subBreeds = await getSubBreeds(breeds[0]);
        console.log(`\nAufgabe 5: Subrassen von ${breeds[0]}:`, subBreeds);

    } catch (error) {
        console.error("Error:", error);
    }
})();
