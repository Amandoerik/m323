// Funktion wird in eine Variable gespeichert
var adder = function (number1, number2) {
    return number1 + number2;
};

adder(1, 6); // Aufruf der Funktion

// Funktion in Funktion
function forEach(list, fn) {
    for (let v of list) {
        fn(v); // hier wird die als argument übergebene Funktion aufgerufen
    }
}

forEach([1, 2, 3, 4, 5], function each(val) {
    console.log(val);
});

// Die Konsolenausgabe ist: 1 2 3 4 5

// -------------------------------------------------------------------------------------------------------

function outerFunction() {
    return function innerFunction(msg) {
        return msg.toUpperCase();
    };
}

var myFunction = outerFunction();

console.log(myFunction("gibz")); // gibz wird zu GIBZ

// -------------------------------------------------------------------------------------------------------

function outerFunction2() {
    return callerFunction(function innerFunction(msg){
        return msg.toUpperCase();
    });
}

function callerFunction(func) {
    return func("gibz");
}

console.log(outerFunction2());          // gibz wird zu GIBZ

