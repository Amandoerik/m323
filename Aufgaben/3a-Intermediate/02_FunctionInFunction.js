// Erstellen Sie eine Funktion welche eine Funktion zurückgibt. Diese soll Sinngemäss "Hello World" ausgeben.

function giveBackFunction() {
    return function helloWorld() {
        console.log("Hello World");
    };
}

giveBackFunction()();

// -------------------------------------------------------------------------------------------------------
// Erstellen Sie eine Zähler Funktion. Beim ersten Aufruf soll "1" zurückgegeben werden, beim Zweiten "2", Dritten "3" usw. Sie sollen keine Schleife verwenden.

function createCounter() {
    let count = 0;

    function countPlusOne() {
        return ++count;
    };

    return countPlusOne;
}
const myCounter = createCounter();

console.log(myCounter()); // 1
console.log(myCounter()); // 2
console.log(myCounter()); // 3

// -------------------------------------------------------------------------------------------------------
// Erstellen Sie eine Funktion die zur Addition von Zahlen verwendet werden kann. Bei jedem Aufruf mit einem Parameter (Zahl) wird die entsprechende Zahl dazugerechnet.

function createAdder() {
    let num1 = 0;

    function adder(num2) {
        return (num1 += num2);
    }

    return adder;
}
const myAdder = createAdder();

console.log(myAdder(5)); // 5
console.log(myAdder(10)); // 15
console.log(myAdder(200)); // 215

// -------------------------------------------------------------------------------------------------------
// Erstellen Sie eine Funktion, welcher ein Ausgangswert (z.B. 10) übergeben werden kann. Wenn man die Funktion in eine Variable speichert und diese dann mit einem weiteren Parameter ausführt, soll dieser mit dem ursprünglichen Wert multipliziert werden.

function multiplier(factor) {
    return function (number) {
        return factor * number;
    };
}

var multiplyBy2 = multiplier(2);
console.log(multiplyBy2(5)); // 10
console.log(multiplyBy2(10)); // 20

var multiplyBy32 = multiplier(32);
console.log(multiplyBy32(5)); // 160
console.log(multiplyBy32(10)); // 320
