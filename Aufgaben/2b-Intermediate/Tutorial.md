# Currying in JavaScript

Currying ist eine Programmierungstechnik, bei der eine Funktion, die mehrere Parameter erfordert, in eine Sequenz von Funktionen umgewandelt wird, die jeweils einen Parameter akzeptieren.

## Grundlagen

Das Currying funktioniert nach einer Reihenfolge. Man kann dies mit einem Rezept vergleichen. Bei einem Rezept werden die Zutaten nacheinander hinzugefügt, anstatt alle auf einmal hinzuzufügen. In einer "normalen" Funktion werden alle Parameter auf einmal hinuzgefügt. In einer curried Funktion geschieht dies nacheinander.

## Vorteile von Currying?

Lesbarkeit: Curried Funktionen können Code lesbarer und aussagekräftiger machen, indem sie komplexe Logik in kleinere, besser handhabbare Teile zerlegen. Dies kann es für Entwickler einfacher machen, den Code zu verstehen und zu pflegen.

Flexibilität: Currying ermöglicht eine größere Flexibilität bei der Zusammensetzung von Funktionen. So können Funktionen auf unterschiedliche Weise kombiniert und wiederverwendet werden, wodurch der Code besser an veränderte Anforderungen angepasst werden kann.

Übersichtlichkeit: Currying hilft dabei, Funktionen zu erstellen, die klar zeigen, was benötigt wird und was sie tun.

Keine Redundanz: Mit Currying kann redundanter code vermieden werden, indem es ermöglicht wird spezialisierte Funktionen zu erstellen, die eine gemeinsame Funktionalität haben.

### Wann wird Currying verwendet?

Wiederverwendbare Funktionen: Um Funktionen in verschiedenen Kontexten mit unterschiedlichen Parametern zu verwenden, hilft Currying dabei, flexiblere und wiederverwendbare Funktionen zu erstellen.

Teilweise Anwendung von Funktionen: Currying ermöglicht die Vorbereitung einer Funktion mit einigen Argumenten und die spätere Vervollständigung. Das ist nützlich, wenn nicht alle Informationen auf einmal verfügbar sind.

Klarer, modularer Code: Currying kann den Code einfacher und verständlicher machen, indem Funktionen in kleinere, spezifischere Teile aufgeteilt werden.

## Beispiel

```javascript
function summe(a) {
  return function(b) {
    return function(c) {
      return a + b + c;
    };
  };
}

const summandA = summe(2);
const summandB = summandA(4);
const Summe = summandB(3); // Ergebnis ist 9
```

In diesem Beispiel wird die Funktion summe schrittweise curried, indem sie ihre Argumente nach und nach erhält. Zuerst wird a übergeben, dann b und schließlich c. Das Endergebnis ist die Summe der drei Zahlen.
