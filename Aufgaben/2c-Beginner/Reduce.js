const numbers = [1, 2, 3, 4, 5];
const names = ['Asabeneh', 'Mathias', 'Elias', 'Brook'];

// Gibt die Summe aller Zahlen eines Arrays zurück
function sumNumbers(numbers) {
    return numbers.reduce((sum, number) => {
        return sum + number;
    }, 0)
}

// Gibt die Summe aller Zahlen eines Arrays zurück, beginnend mit 10
function sumNumbersPlus10(numbers) {
    return numbers.reduce((sum, number) => {
        return sum + number;
    }, 10)
}

// Gibt alle Namen eines Arrays in einem String zurück
function putNamesInString(names) {
    return names.reduce((sentence, name) => {
        return `${sentence} ${name}`;
    }, 'The names are:')
}


console.log(sumNumbers(numbers));
console.log(sumNumbersPlus10(numbers));
console.log(putNamesInString(names));


// Was ist macht die Funktion reduce()?
// Die reduce() Funktion durchläuft jedes Element eines Arrays und gibt am Ende ein einzelnes Ergebnis zurück. Dieses Ergebnis ist dann kein Array mehr, sondern ein einzelner Wert. 
// Der Erste Parameter beschreibt immer den Ausgangswert. Standardmäßig ist dieser 0. In der zweiten Funktion ist als Beispiel der Ausgangswert 10 und somit ist das Resultat am Ende 25 und nicht 15.