const numbers = [1, 2, 3, 4, 5];
const names = ['Asabeneh', 'Mathias', 'Elias', 'Brook'];

// Gibt alle Zahlen eines Arrays jeweils verdoppelt zurück
function double(numbers) {
    return numbers.map((number) => {
        return number * 2;  
    })
}

// Gibt alle Namen eines Arrays mit einem Hello davor zurück
function addHello(names) {
    return names.map((name) => {
        return `Hello ${name}`;
    })
}


console.log(double(numbers));
console.log(addHello(names));

// Was ist macht die Funktion map()?
// Die map() Funktion durchläuft jedes Element eines Arrays und gibt eine neue Version des Arrays zurück. 