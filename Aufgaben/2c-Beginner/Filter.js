const numbers = [1, 2, 3, 4, 5];
const names = ['Asabeneh', 'Mathias', 'Elias', 'Brook', 'Daniel', 'John', 'David', 'dan'];

// Gibt alle geraden Zahlen eines Arrays zurück
function isEven(number) {
    return number.filter((number) => {
        return number % 2 === 0;
    })
}

// Gibt alle Namen zurück, die mit einem grossen D beginnen
function nameStartsWithD(name) {
    return name.filter((name) => {
        return name.startsWith('D');
    })
}

console.log(isEven(numbers));
console.log(nameStartsWithD(names));


// Was ist macht die Funktion filter()?
// Die filter() Funktion durchläuft jedes Element eines Arrays und gibt ein neues Array zurück, das nur die Elemente enthält, die die Bedingung erfüllen. Also so wie es der Name schon sagt, filtert es die Elemente des Arrays.