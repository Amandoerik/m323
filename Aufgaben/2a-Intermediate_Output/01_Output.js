function add() {}

function sub() {
  return;
}

function tree() {
  return undefined;
}

console.log(add()); // undefined
console.log(sub()); // undefined
console.log(tree()); // undefined