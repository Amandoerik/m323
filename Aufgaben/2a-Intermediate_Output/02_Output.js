function calc(x) {
    if (x < 10) return x + 10;

    var y = x / 2;

    if (y < 5) {
        if (x % 2 ** 0) return x;
    }

    if (y > 10) return y;

    return x;
}

console.log(calc(5)); // 15
console.log(calc(20)); // 10
console.log(calc(7)); // 17
console.log(calc(45)); // 22.5
console.log(calc(12)); // 12

function newCalc(x) {
	let y = x / 2;
	if (x < 10) {
		x + 10;
	} 
	else if (y > 10) {
		x = y;
	}

    return x;
}

console.log(newCalc(5)); // 15
console.log(newCalc(20)); // 10
console.log(newCalc(7)); // 17
console.log(newCalc(45)); // 22.5
console.log(newCalc(12)); // 12
