// Beispiel der Aufgabe

// function sum(listOfNumers) {
//     var sum = 0;
//     for (let i = 0; i < listOfNumers.length; i++) {
//         if (!listOfNumers[i]) listOfNumers[i] = 0;
//         sum = sum + listOfNumers[i];
//     }
//     return sum;
// }
// var numbers = [1, , 3, 9, 27, 64];
// console.log(sum(numbers)); // 104


// Lösung der Aufgabe

function sum(listOfNumers) {
    let sum = 0;
    for (let i = 0; i < listOfNumers.length; i++) {
        if (listOfNumers[i] != undefined) {
            sum = sum + listOfNumers[i];
        }
    }
    return sum;
}
let numbers = [1, , 3, 9, 27, 64];
console.log(sum(numbers)); // 104


// Erklärung der pure function befindet sich in der txt-Datei "03_Output.txt"