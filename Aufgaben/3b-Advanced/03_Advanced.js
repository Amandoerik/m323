// ------ Aufgabe 3 ------
function words(str) {
    return String(str)
        .toLowerCase()
        .split(/\s|\b/)
        .filter(function alpha(v) {
            return /^[\w]+$/.test(v);
        });
}

function unique(list) {
    var uniqList = [];

    for (let v of list) {
        if (uniqList.indexOf(v) === -1) {
            uniqList.push(v);
        }
    }

    return uniqList;
}

var compose =
    (...fns) =>
        result => {
            var list = [...fns];

            while (list.length > 0) {
                result = list.pop()( result );
            }

            return result;
        };


function skipShortWords(words) {
    var filteredWords = [];

    for (let word of words) {
        if (word.length > 4) {
            filteredWords.push( word );
        }
    }

    return filteredWords;
}


var text = "To compose two functions together, pass the output of the first function call as the input of the second function call.";
var secondText = "Die Watch 2 Pro bietet dir ein optimales smartes Erlebnis, das du bequem am Handgelenk tragen kannst. \
Laden Sie Ihre Lieblings-Apps mit Wear OS herunter und erfahren Sie mit jedem Tragen mehr über Ihre Gesundheit und Fitness. \
Die Watch 2 Pro ist mit ihrem 46-mm-Zifferblatt mit Pariser Hobnail-Lünette und einem hochwertigen, fein polierten Edelstahlgehäuse wunderschön im Stil einer klassischen Armbanduhr gestaltet.";

var biggerWords = compose( skipShortWords, unique, words );

var wordsUsed = biggerWords( text );
var wordsUsed2 = biggerWords( secondText );

console.log(wordsUsed);
// ["compose","functions","together","output","first",
// "function","input","second"]

console.log(wordsUsed2);
// ["watch","bietet","optimales","smartes","erlebnis", etc.]
