// ------ Aufgabe 4 ------

function skipLongWords(list) {
    return list.filter(word => word.length <= 4);
}

function partialRight(fn, ...presetArgs) {
    return function partiallyApplied(...laterArgs) {
        return fn(...laterArgs, ...presetArgs);
    };
}

function compose(...fns) {
    return function composed(result) {
        return fns.reduceRight((acc, fn) => fn(acc), result);
    };
}

function unique(list) {
    return [...new Set(list)];
}

function words(text) {
    return text.split(" ");
}

var text = "compose functions together to output the first function input second";
var text2 = "JavaScript is a popular programming language used for web development.";

var filterWords = partialRight(compose, unique, words);

var skipShortWords = function(list) {
    return list.filter(word => word.length > 4);
};

var biggerWords = filterWords(skipShortWords);
var shorterWords = filterWords(skipLongWords);

console.log(biggerWords(text));
// Output: ["compose","functions","together","output","first","function","input","second"]

console.log(shorterWords(text));
// Output: ["to","the"]¨

console.log(biggerWords(text2));
// Output: ["JavaScript","popular","programming","language","development."]

console.log(shorterWords(text2));
// Output: ["is","a","for","web"]
