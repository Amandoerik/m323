function sortNumbers(numbers) {
    return numbers.sort((a, b) => a - b);
}

function testSortNumbers(testCases) {
    for (let testCase of testCases) {
        if (sortNumbers(testCase.numbers).toString() !== testCase.expected.toString()) {
            console.error(`Test failed for "${testCase.numbers}"`);
        }
        if (sortNumbers(testCase.numbers).toString() === testCase.expected.toString()) {
            console.log(`Test passed for "${testCase.numbers}"`);
        }
    }
}

const testCases = [
    {numbers: [], expected: []},
    {numbers: [5], expected: [5]},
    {numbers: [1, 2, 3, 4, 5], expected: [1, 2, 3, 4, 5]},
    {numbers: [5, 4, 3, 2, 1], expected: [1, 2, 3, 4, 5]},
    {numbers: [3, 2, 4, 1, 2, 5, 4, 3], expected: [1, 2, 2, 3, 3, 4, 4, 5]},
    {numbers: [1, -1], expected: [-1, 1]},
    {numbers: [1.5, 0.7, -1.2, 1.7, 3.1], expected: [-1.2, 0.7, 1.5, 1.7, 3.1]},
    {numbers: [-1, -3, -2, 0, 2, 1], expected: [-3, -2, -1, 0, 1, 2]},
];

testSortNumbers(testCases);