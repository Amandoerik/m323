console.log("Start");

function asynchronerProzess(callback) {
  setTimeout(function() {
    console.log("Asynchrone Operation abgeschlossen");
    callback();
  }, 2000);
}

asynchronerProzess(function() {
  console.log("Fortsetzung des Codes nach asynchroner Operation");
});

console.log("Ende");
