function fibonacciRecursive(n) {
    if (n <= 1) {
        return n;
    }
    return fibonacciRecursive(n - 1) + fibonacciRecursive(n - 2);
}

function testFibonacciRecursive(testCases) {
    for (let testCase of testCases) {
        if (fibonacciRecursive(testCase.input) !== testCase.expected) {
            console.error(`Test failed for "${testCase.input}"`);
        }
        if (fibonacciRecursive(testCase.input) === testCase.expected) {
            console.log(`Test passed for "${testCase.input}"`);
        }
    }
}

function performanceTestFibonacciRecursive(n) {
    const start = performance.now();
    fibonacciRecursive(n);
    const end = performance.now();
    return end - start;
}


/* Alternative Lösung für performanceTestFibonacciRecursive, allerdings ist die obige Lösung noch ein Stück präziser
**
function performanceTestFibonacciRecursive(n) {
    console.time("fibonacciRecursive");
    const result = fibonacciRecursive(n);
    console.timeEnd("fibonacciRecursive");
    console.log(`Fibonacci(${n}) = ${result}`);
}*/

const testCases = [
    { input: 0, expected: 0 },
    { input: 1, expected: 1 },
    { input: 2, expected: 1 },
    { input: 3, expected: 2 },
    { input: 6, expected: 6 },
    { input: 21, expected: 10946 },
    { input: 40, expected: 102334155 },
];

testFibonacciRecursive(testCases);
console.log(`Performance-Test für Fibonacci(40): ${performanceTestFibonacciRecursive(40)} ms`);