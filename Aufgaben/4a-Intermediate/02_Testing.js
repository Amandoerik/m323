function panlindrome(str) {
    let reversedStr = str.toLowerCase().split('').reverse().join('');
    return str === reversedStr;
}

function testPanlindrome(testCases) {
    for (let testCase of testCases) {
        if (panlindrome(testCase.str) !== testCase.expected) {
            console.error(`Test failed for "${testCase.str}"`);
        }
        if (panlindrome(testCase.str) === testCase.expected) {
            console.log(`Test passed for "${testCase.str}"`);
        }
    }
    
}

const testCases = [
    {str: 'Anna', expected: true},
    {str: '1221', expected: true},
    {str: '123456789', expected: false},
    {str: 'Lagerregal', expected: true},
    {str: 'Barcelona', expected: true},
]

testPanlindrome(testCases);