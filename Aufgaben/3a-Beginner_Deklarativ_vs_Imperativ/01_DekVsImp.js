function foo(params) {
  var name = params[0];
  var age = params[1];
  var args = params.slice(2);
  // any logic
}

console.log(foo(["Manu", 20]));

// -------------------------------------

function foo(name, age) {
  // any logic
}

console.log(foo("Manu", 20));

// -------------------------------------

function foo({ name, age }) {
  // any logic
}

console.log(foo({ name: "Manu", age: 20 }));
